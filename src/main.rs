use hyper::StatusCode;
use sqlx::{postgres::PgPoolOptions, Pool, Postgres, PgPool};
use axum::{
    extract::{Extension, Path},
    routing::{get, post},
    Router, Json,
};
use axum_macros::debug_handler;
use std::{net::SocketAddr};

#[tokio::main]
async fn main() -> Result<(), sqlx::Error> {
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect("postgresql://dboperator:operatorpass123@localhost:5243/postgres").await?;
    
    // build our application with some routes
    let app = app(pool);

    // run it with hyper
    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
    Ok(())
}


fn app(pool: Pool<Postgres>) -> Router {
    Router::new()
        .route(
            "/users/:id",
            get(get_user).delete(delete_user)
        )
        .route(
            "/users", 
            post(post_user).patch(patch_user)
        )
        .layer(Extension(pool))
}

// A wrapper type for all requests/responses from these routes.
#[derive(serde::Serialize, serde::Deserialize)]
struct UserBody<T> {
    user: T,
}

#[derive(sqlx::FromRow, serde::Serialize, serde::Deserialize, Debug, sqlx::Type)]
pub struct User {
    username: String,
    email: String,
    password: String,
}

#[derive(serde::Deserialize)]
struct NewUser {
    username: String,
    email: String,
    password: String,
}

#[derive(serde::Deserialize)]
struct UpdateUser {
    id: u32,
    username: String,
    email: String,
    password: String,
}

#[debug_handler]
pub async fn get_user(
    Path(id): Path<u32>,
    Extension(pool): Extension<PgPool>,
) -> Result<Json<User>, (StatusCode, String)> {
    let user = sqlx::query_scalar(
        "select email, username, password 
        from users
        where id = $1
        ")
        .bind(id)
        .fetch_one(&pool)
        .await
        .map_err(internal_error);
    println!("{user:?}");
    match user{
        Ok(res) => Ok(Json(res)),
        Err(error) => Err(error),
    }
   
}

async fn post_user(
    Extension(pool): Extension<PgPool>,
    Json(req): Json<UserBody<NewUser>>,
) -> Result<String, (StatusCode, String)> {
    sqlx::query_scalar("insert into users (username, password, email) values ($1, $2, $3) returning id")
        .bind(&req.user.username)
        .bind(&req.user.email)
        .bind(&req.user.password)
        .fetch_one(&pool)
        .await
        .map_err(internal_error)
}

async fn patch_user(
    Extension(pool): Extension<PgPool>,
    Json(req): Json<UserBody<UpdateUser>>,
) -> Result<String, (StatusCode, String)> {
    sqlx::query_scalar(        
        "update user
        set username = coalesce($1, user.username)
            password = coalesce($2, user.password)
            email = coalesce($3, user.email)
            where id = $4
            returning id")
        .bind(&req.user.username)
        .bind(&req.user.email)
        .bind(&req.user.password)
        .bind(&req.user.id)
        .fetch_one(&pool)
        .await
        .map_err(internal_error)
}

async fn delete_user(
    Path(id): Path<u32>,
    Extension(pool): Extension<PgPool>,
) -> Result<String, (StatusCode, String)> {
    sqlx::query_scalar(
        "delete from users
        where id = $1")
        .bind(id)
        .fetch_one(&pool)
        .await
        .map_err(internal_error)
}

/// Utility function for mapping any error into a `500 Internal Server Error`
/// response.
fn internal_error<E>(err: E) -> (StatusCode, String)
where
    E: std::error::Error,
{
    (StatusCode::INTERNAL_SERVER_ERROR, err.to_string())
}